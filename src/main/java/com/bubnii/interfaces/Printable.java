package com.bubnii.interfaces;

public interface Printable {
    void print();
}
