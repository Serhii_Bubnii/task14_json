package com.bubnii.model.entity;

import java.util.ArrayList;
import java.util.List;

public class Bank {

    private int deposit;
    private String name;
    private String country;
    private List<Type> type = new ArrayList<>();
    private String depositor;
    private int accountId;
    private int amountOnDeposit;
    private double profitability;
    private Date date;

    public Bank() {
    }

    public Bank(int deposit, String name, String country, List<Type> type, String depositor,
                int accountId, int amountOnDeposit, double profitability, Date date) {
        this.deposit = deposit;
        this.name = name;
        this.country = country;
        this.type = type;
        this.depositor = depositor;
        this.accountId = accountId;
        this.amountOnDeposit = amountOnDeposit;
        this.profitability = profitability;
        this.date = date;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Type> getTypes() {
        return type;
    }

    public void setTypes(List<Type> type) {
        this.type = type;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(int amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public double getProfitability() {
        return profitability;
    }

    public void setProfitability(double profitability) {
        this.profitability = profitability;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "deposit=" + deposit +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", type=" + type +
                ", depositor='" + depositor + '\'' +
                ", accountId=" + accountId +
                ", amountOnDeposit=" + amountOnDeposit +
                ", profitability=" + profitability +
                ", date=" + date +
                '}';
    }
}
