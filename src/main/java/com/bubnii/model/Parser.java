package com.bubnii.model;

import com.bubnii.model.entity.Bank;
import com.bubnii.model.parser.ParserGSON;
import com.bubnii.model.parser.ParserJackson;
import com.bubnii.model.parser.ValidatorJSON;
import com.bubnii.view.MessagePrinter;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Parser {

    private File fileJSON = new File("src\\main\\resources\\json\\bankJSON.json");
    private File fileJSONSchema = new File("src\\main\\resources\\json\\bankJSONSchema.json");
    private ParserGSON parserGSON;
    private ParserJackson parserJackson;
    private MessagePrinter printer;

    public Parser(){
        this.parserGSON = new ParserGSON();
        this.parserJackson = new ParserJackson();
        this.printer = new MessagePrinter();
    }

    public void outValidator() {
        try {
            if (ValidatorJSON.validate(fileJSON, fileJSONSchema)) {
                printer.printMessage("JSON");
                printList(parserJackson.getBankListJackson(fileJSON));
                printer.printMessage("GSON");
                printList(parserGSON.getBankListGSON(fileJSON));
            }else {
                printer.printMessage("Not valid!");
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }

    public void outParserGSON(){
        try {
            printer.printMessage("GSON");
            printList(parserGSON.getBankListGSON(fileJSON));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void outParserJSON(){
        printer.printMessage("JSON");
        printList(parserJackson.getBankListJackson(fileJSON));
    }

    private static void printList(List<Bank> bankList) {
        Collections.sort(bankList, new BankComparator());
        for (Bank plane : bankList) {
            System.out.println(plane);
        }
    }
}
