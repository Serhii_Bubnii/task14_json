package com.bubnii.controller;

import com.bubnii.model.Parser;

public class Controller {

    private Parser parser;

    public Controller() {
        this.parser = new Parser();
    }

    public void runParserGSON() {
        parser.outParserGSON();
    }

    public void runParserJSON() {
        parser.outParserJSON();
    }

    public void runValidator() {
        parser.outValidator();
    }
}
