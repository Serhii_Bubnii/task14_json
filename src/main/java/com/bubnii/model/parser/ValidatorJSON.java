package com.bubnii.model.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class ValidatorJSON {

    public static boolean validate(File json, File schema) throws IOException, ProcessingException {
        final JsonNode jsonNode = JsonLoader.fromFile(json);
        final JsonNode schemaNode = JsonLoader.fromFile(schema);
        final JsonSchemaFactory schemaFactory = JsonSchemaFactory.byDefault();
        JsonValidator jsonValidator = schemaFactory.getValidator();
        ProcessingReport processingReport = jsonValidator.validate(schemaNode, jsonNode);
        return processingReport.isSuccess();
    }
}
