package com.bubnii.model.parser;

import com.bubnii.model.entity.Bank;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParserJackson {

    private ObjectMapper objectMapper;

    public ParserJackson() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Bank> getBankListJackson(File fileJSON) {
        List<Bank> banks = new ArrayList<>();
        try {
            banks = Arrays.asList(objectMapper.readValue(fileJSON, Bank[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return banks;
    }
}
