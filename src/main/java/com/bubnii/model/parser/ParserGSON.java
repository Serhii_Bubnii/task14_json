package com.bubnii.model.parser;

import com.bubnii.model.entity.Bank;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class ParserGSON {

    private Gson gson;

    public ParserGSON() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        this.gson = gsonBuilder.create();
    }

    public List<Bank> getBankListGSON(File json) throws FileNotFoundException {
        return Arrays.asList(gson.fromJson(new FileReader(json), Bank[].class));
    }
}
